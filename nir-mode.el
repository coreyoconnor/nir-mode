;;; package --- nir-mode
;;; Commentary:

;;; Code:

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.hnir\\'" . nir-mode))

(defvar nir-mode-hook nil)

(defvar nir-mode-map (make-sparse-keymap))

(defconst nir-font-lock-keywords-1
  (list
   '("\\(c\\(?:lass\\|onst\\)\\|def\\|module\\|struct\\|trait\\|var\\)" . font-lock-keyword-face)
   )
  "basic highlighting for human-readable NIR (hnir)."
  )

(defvar nir-font-lock-keywords nir-font-lock-keywords-1
  "Default highlighting expressions for NIR mode.")

(defun nir-mode ()
  "Major mode nir-mode."
  (interactive)
  (kill-all-local-variables)
  (set (make-local-variable 'font-lock-defaults) '(nir-font-lock-keywords))
  (setq major-mode 'nir-mode)
  (setq mode-name "NIR")
  (run-hooks 'nir-mode-hook)
  )

(provide 'nir-mode)

;;; nir-mode.el ends here
